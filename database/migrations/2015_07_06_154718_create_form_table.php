<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('visit')->default(0);
            $table->string('visit_weekday', 24);
            $table->string('visit_time', 12);
            $table->tinyInteger('patient_leaflets')->default(0);
            $table->tinyInteger('information_pack')->default(0);
            $table->string('name');
            $table->string('practice_address');
            $table->string('contact_number');
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('form_requests');
    }
}
