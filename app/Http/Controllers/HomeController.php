<?php namespace App\Http\Controllers;

use App\FormRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $showPopup = true;

        if ($request->session()->has('popup')) {
            $request->session()->remove('popup');
            $showPopup = false;
        }

        return view('pages.index')->with([
            'pageClass' => 'home',
            'showPopup' => $showPopup
        ]);
    }

    public function banners(Request $request)
    {
        $request->session()->put('popup', true);

        return redirect()->to('/');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function save(Request $request)
    {
        $formRequest = new FormRequest;

        $formRequest->visit = $request->get('visit');
        $formRequest->visit_weekday = $request->get('visitWeekday');
        $formRequest->visit_time = $request->get('visitTime');
        $formRequest->patient_leaflets = $request->get('leaflets');
        $formRequest->information_pack = $request->get('information');
        $formRequest->name = $request->get('name');
        $formRequest->practice_address = $request->get('practiceAddress');
        $formRequest->contact_number = $request->get('contactNumber');
        $formRequest->email = $request->get('email');

        if ($formRequest->save()) {
            // send notification email
            $this->sendNotificationEmail($this->prepareEmailData($formRequest));

            return response()->json(['status' => 'OK', 'msg' => 'Request saved']);
        }

        return response()->json(['status' => 'BAD', 'msg' => 'Failed saving request']);
    }

    /**
     * Send notification email
     *
     * @param array $data
     * @return bool
     */
    public function sendNotificationEmail($data)
    {
        Mail::send('emails.form-request-notification', $data, function ($message) {
            $message->to(env('email'), 'GetOnWith')->subject('GetOnWith new visit request');

            if (count(Mail::failures()) == 0) {
                return true;
            }
        });

        return false;
    }

    /**
     * Prepare the data to be sent via email
     *
     * @param array $formRequestData
     * @return array $converted
     */
    public function prepareEmailData($formRequestData)
    {
        $converted = [];
        $converted['visit'] = $this->binaryToYesOrNo($formRequestData['visit']);
        $converted['visitDay'] = $this->convertDayNumberToDayName($formRequestData->visit_weekday, $formRequestData['visit_time']);
        $converted['patientLeaflets'] = $this->binaryToYesOrNo($formRequestData['patient_leaflets']);
        $converted['informationPack'] = $this->binaryToYesOrNo($formRequestData['information_pack']);
        $converted['name'] = $formRequestData['name'];
        $converted['practiceAddress'] = $formRequestData['practice_address'];
        $converted['contactNumber'] = $formRequestData['contact_number'];
        $converted['email'] = $formRequestData['email'];

        return $converted;
    }

    /**
     * Return the value based on $n variable
     *
     * @param int $n
     * @return string
     */
    public function binaryToYesOrNo($n)
    {
        if ($n == 1) {
            return 'Yes';
        } elseif ($n == 0) {
            return 'No';
        }

        return 'N/A';
    }

    /**
     * Convert day numbers to the name of the days
     * and add visit times
     *
     * @param $dayNumber
     * @param $visitTime
     * @return array|string
     */
    public function convertDayNumberToDayName($dayNumber, $visitTime)
    {
        $dayNumbers = json_decode($dayNumber, true);
        $visitTimes = json_decode($visitTime, true);
        $days = [];
        $weekdays = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday'
        ];

        if (count($dayNumbers) === 0) {
            $days = "N/A";
        }

        foreach ($dayNumbers as $key => $dayNumber) {
            if (array_key_exists($dayNumber, $days)) {
                $days[$dayNumber] .= '/' . $visitTimes[$key];
            } else {
                $days[$dayNumber] = $weekdays[$dayNumber] . ' - ' . $visitTimes[$key];
            }
        }

        return $days;
    }
}
