<?php

Route::get('/', 'HomeController@index');
Route::get('banners', 'HomeController@banners');
Route::post('save', 'HomeController@save');
