<script src="{{ asset('assets/js/vendor/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/vendor/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/vendor/jquery.visible.min.js') }}"></script>
<script src="{{ asset('assets/js/vendor/jquery.scrollbar.js') }}"></script>
<script src="{{ asset('assets/js/vendor/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('assets/js/vendor/jquery.jscrollpane.min.js') }}"></script>
<script src="{{ asset('assets/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/vendor/modernizr.min.js') }}"></script>
<script src="{{ asset('assets/js/vendor/detectizr.min.js') }}"></script>

<script src="{{ asset('assets/js/modules/App.js') }}"></script>
<script src="{{ asset('assets/js/modules/Slider.js') }}"></script>
<script src="{{ asset('assets/js/modules/Radio.js') }}"></script>

<script>
    $(function () {
        var app = new App();

        app.init({
            preload: [
                '{{ asset('assets/img/getonwith-dad3.jpg') }}'
            ]
        });

        (new Slider).init([
            $('.slider-1'),
            $('.slider-2')
        ]);

        if (window.location.hash === '#pi') {
            $('.welcome').hide();
            $('.home').show();
            $('body').addClass('background-image-on');
            $('#prescribing-modal').modal('show');
        }

        $("#request-img")
                .mouseover(function () {
                    $(this).stop().animate({
                        width: '104%',
                        'margin-left': '-4px'
                    }, 150, function () {
                        $(this).stop().animate({
                            width: '100%',
                            'margin-left': '0px'
                        }, 150);
                    });
                });

        function centerModals($element) {
            var $modals;

            if ($element.length) {
                $modals = $element;
            } else {
                $modals = $('.modal-vcenter:visible');
            }

            $modals.each(function (i) {
                var $clone = $(this).clone().css('display', 'block').appendTo('body');
                var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
                top = top > 0 ? top : 0;
                $clone.remove();
                $(this).find('.modal-content').css("margin-top", top);
            });
        }

        $('.modal-vcenter').on('show.bs.modal', function (e) {
            centerModals($(this));
        });

        $(window).on('resize', centerModals);
    });
</script>

