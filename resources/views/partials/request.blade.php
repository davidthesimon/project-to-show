<div class="modal fade modal-vcenter" id="request-modal" tabindex="-1" role="dialog" aria-labelledby="request-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="{{ asset('assets/img/x.jpg') }}"></button>
            </div>
            <div class="modal-body">

                <div id="request-thank-you">
                    <img src="{{ asset('/assets/img/request/thank-you-for-getting-in-touch.jpg') }}">
                </div>

                <div class="request-a-visit-checkbox">
                    <a href="#" id="checkbox1">
                        <div class="checkbox1 active"></div>
                    </a>
                </div>

                <div class="request-a-visit">
                    <img src="{{ asset('assets/img/request/request-a-visit.png') }}">
                </div>

                <div class="preferred-times">
                    <img src="{{ asset('assets/img/request/preferred-times.png') }}">
                </div>

                <div class="table-img">
                    <img src="{{ asset('assets/img/request/table.png') }}">
                    <div class="checkboxes-table" id="checkbox-table-1"><div class="spacer"><img src="{{ asset('assets/img/request/spacer.png') }}"></div></div>
                    <div class="checkboxes-table" id="checkbox-table-2"><div class="spacer"><img src="{{ asset('assets/img/request/spacer.png') }}"></div></div>
                    <div class="checkboxes-table" id="checkbox-table-3"><div class="spacer"><img src="{{ asset('assets/img/request/spacer.png') }}"></div></div>
                    <div class="checkboxes-table" id="checkbox-table-4"><div class="spacer"><img src="{{ asset('assets/img/request/spacer.png') }}"></div></div>
                    <div class="checkboxes-table" id="checkbox-table-5"><div class="spacer"><img src="{{ asset('assets/img/request/spacer.png') }}"></div></div>
                    <div class="checkboxes-table" id="checkbox-table-6"><div class="spacer"><img src="{{ asset('assets/img/request/spacer.png') }}"></div></div>
                    <div class="checkboxes-table" id="checkbox-table-7"><div class="spacer"><img src="{{ asset('assets/img/request/spacer.png') }}"></div></div>
                    <div class="checkboxes-table" id="checkbox-table-8"><div class="spacer"><img src="{{ asset('assets/img/request/spacer.png') }}"></div></div>
                    <div class="checkboxes-table" id="checkbox-table-9"><div class="spacer"><img src="{{ asset('assets/img/request/spacer.png') }}"></div></div>
                    <div class="checkboxes-table" id="checkbox-table-10"><div class="spacer"><img src="{{ asset('assets/img/request/spacer.png') }}"></div></div>
                    {{--<div class="overlay"></div>--}}
                </div>

                <div class="getonwith-placebo-checkbox">
                    <a href="#" id="checkbox2">
                        <div class="checkbox2 active"></div>
                    </a>
                </div>

                <div class="information-pack-checkbox">
                    <a href="#" id="checkbox3">
                        <div class="checkbox3 active"></div>
                    </a>
                </div>

                <div class="receive">
                    <img src="{{ asset('assets/img/request/receive.png') }}">
                </div>

                <div class="info-area">
                    <div class="form-errors">
                        N.B. You are not obligated to see a representative in order to receive patient
                        information materials
                    </div>

                    <img src="{{ asset('assets/img/request/info-area.png') }}">
                </div>

                <div class="form-container">
                    <form id="request-form" name="form-request" method="post" action="#">
                        <div class="form-group">
                            <label><img src="{{ asset('assets/img/request/text-name.png') }}"></label>
                            <input name="name" id="name" class="inputs" type="text">
                        </div>

                        <div class="form-group">
                            <label><img src="{{ asset('assets/img/request/text-address.png') }}"></label>
                            <input name="address" id="address" class="inputs" type="text">
                        </div>

                        <div class="form-group">
                            <label><img src="{{ asset('assets/img/request/text-phone.png') }}"></label>
                            <input name="phone" id="phone" class="inputs" type="text">
                        </div>

                        <div class="form-group">
                            <label><img src="{{ asset('assets/img/request/text-email.png') }}"></label>
                            <input name="email" id="email" class="inputs" type="email">
                        </div>
                    </form>
                </div>

                <div id="ajax-error"></div>

                <img id="ajax-loader" src="{{ asset('/assets/img/ajax-loader.gif') }}">

                <div class="submit-container">
                    <input type="submit" value="" id="request-submit" class="submit-btn" name="submit">
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->