@extends('layouts.default')


@section('content')
    <div class="content">
        @if($showPopup)
            <div class="welcome">
                <div class="besins-logo">
                    <img src="{{ asset('assets/img/welcome/besins-logo.png') }}">
                </div>

                <div class="welcome-box">
                    <div class="inner-box">
                        <div class="to-continue">
                            <img src="{{ asset('assets/img/welcome/to-continue.png') }}">
                        </div>

                        <div class="radio-button">
                            <a href="#" id="top-radio">
                                <div class="top-radio active"></div>
                            </a>

                            <a href="#" id="bottom-radio">
                                <div class="bottom-radio"></div>
                            </a>
                        </div>

                        <div class="welcome-options">
                            <img id="healthcare-professional" src="{{ asset('assets/img/welcome/welcome-option-healthcare-profession.jpg') }}">

                            <img id="a-patient" src="{{ asset('assets/img/welcome/welcome-option-a-patient.jpg') }}">
                        </div>

                        <div class="getonwith-logo">
                            <img src="{{ asset('assets/img/welcome/getonwith-logo.png') }}">
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="home" style="display: {{ !$showPopup ? 'block;' : 'none;' }}">
            <div class="give-him-back">
                <img src="{{ asset('assets/img/home/give-him-back.png') }}">
            </div>

            <div class="request-a-rep">
                <a href="#" id="request" data-toggle="modal" data-target="#request-modal">
                    <img id="request-img" src="{{ asset('assets/img/home/request-a-rep.png') }}">
                </a>
            </div>

            <div class="prescribing">
                <a href="#" id="prescribing" data-toggle="modal" data-target="#prescribing-modal">
                    <img src="{{ asset('assets/img/home/pi-link.png') }}">
                </a>
            </div>

            <div class="privacy">
                <a href="#" id="privacy" data-toggle="modal" data-target="#privacy-modal">
                    <img src="{{ asset('assets/img/home/privacy-link.png') }}">
                </a>
            </div>

            <div class="copy">
                <img src="{{ asset('assets/img/home/copy.png') }}">
            </div>

            <div class="put-back">
                <img src="{{ asset('assets/img/home/put-back.png') }}">
            </div>

            <div class="slider-container">
                <div class="slider-1 active">
                    <div class="inner">
                        <div class="low-testosterone">
                            <img src="{{ asset('assets/img/home/low-testosterone.png') }}">
                        </div>

                        <div class="list-1">
                            <img src="{{ asset('assets/img/home/list-1.png') }}">
                        </div>
                    </div>
                </div>

                <div class="slider-2">
                    <div class="inner">
                        <div class="getonwith-is">
                            <img src="{{ asset('assets/img/home/getonwith-is.png') }}">
                        </div>

                        <div class="list-2">
                            <img src="{{ asset('assets/img/home/list-2.png') }}">
                        </div>

                        <div class="well-tolerated">
                            <img src="{{ asset('assets/img/home/well-tolerated.png') }}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="logos">
                <img src="{{ asset('assets/img/home/logos.png') }}">
            </div>
        </div>
    </div>
@endsection

@section('script')
    @parent
@endsection