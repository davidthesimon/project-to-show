<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="csrf" content="{{ csrf_token() }}">

    <title>GetOnWith</title>

    <link rel="icon" type="image/png" href="{{ asset('/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

    <script>
        var WWW_ROOT = '{{ url('/') }}';
    </script>

</head>

<body class="{{ !$showPopup ? 'background-image-on' : '' }}">

@include('partials.prescribing')
@include('partials.privacy')
@include('partials.request')

<div id="wrapper">
    <div id="container">
        <div class="container-fluid">
            @yield('content')
        </div>

    </div><!-- #container -->
</div><!-- #wrapper -->

@include('partials.scripts')

@yield('script')

    <script>
        @if(env('APP_ENV') === 'production')
            // Google Analyics
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-44452286-47', 'auto');
            ga('send', 'pageview');
        @endif
    </script>
</body>
</html>